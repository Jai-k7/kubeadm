# Steps to configure k8s 3 node multimaster setup 

Lets assume below are available details of nodes:

| Node |      IP   |
|------|-----------|
| Node1| 142.44.219.118|
| Node2| 142.44.219.119|
| Node3| 142.44.219.120|
| Virtual IP|142.44.219.121|

![Alt text](images/k8s_cluster.jpg)


## Step1: Install and Configure HAProxy Load Balancer with Keepalived

##### Run these commands on all nodes

### 1.1 Configure SELinux

Allow HAProxy to listen on kube-apiserver port 6446:

`$ sudo semanage port -a -t http_cache_port_t 6446 -p tcp`

### 1.2 Install Packages

`$ sudo yum install -y haproxy keepalived psmisc`

### 1.3 Configure HAProxy
Add the `haproxy.cfg` configuration file to /etc/haproxy/haproxy.cfg, keeping in mind that our virtual IP address is 142.44.219.121

add this configuration to all three nodes, then Enable and start the haproxy service:

`$ sudo systemctl enable  haproxy`

`$ sudo systemctl start haproxy`

### 1.4 Configure Keepalived

The host’s kernel needs to be configured to allow a process to bind to a non-local IP address. This is because non-active VRRP nodes will not have the virtual IP configured on any interfaces.

`$ echo "net.ipv4.ip_nonlocal_bind=1" | sudo tee /etc/sysctl.d/ip_nonlocal_bind.conf`

`$ sudo sysctl --system`

Configure the master keepalived node1 by replace `keepalived.conf-master` file's content to `/etc/keepalived/keepalived.conf`

same goes for node2 and node3 with `keepalived.conf-Backup` file,
update priority to 99 in node3 configuration.

Enable and start the keepalived service:

`$ sudo systemctl enable keepalived`

`$ sudo systemctl start keepalived`

## Step2:  Install Docker Packages

##### Run these commands on all nodes

#### 2.1 Install the yum-utils package (which provides the yum-config-manager utility) and set up the stable repository.

`$  sudo yum install -y yum-utils`

`$  sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo`

#### 2.2 Install Docker engine and containerd:

`$ sudo yum install docker-ce docker-ce-cli containerd.io`

#### 2.3 Start and enable service:

`$ sudo systemctl enable docker`

`$ sudo systemctl start docker`

#### 2.4 Create file `/etc/docker/daemon.json` and add following configuration.

```
{
  "exec-opts": ["native.cgroupdriver=systemd"]
} 
```

Restart docker service: `$ sudo systemctl restart docker`

## Step3: Install Kubernetes Packages and Disable Swap

##### Run these commands on all nodes
`$ sudo swapoff -a`

#### 3.1 Set up the repository. Add the following to /etc/yum.repos.d/kubernetes.repo:

```
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
exclude=kubelet kubeadm kubectl
```
exclude all Kubernetes packages from any system upgrades because they have a special process that has to be followed.

#### 3.2 Install kubeadm, kubelet and kubectl:

`$ sudo yum install -y --disableexcludes=kubernetes kubernetes-cni kubelet kubectl kubeadm`

#### 3.3 Enable kubelet service:

`$ sudo systemctl enable kubelet`


##  Step4: Configure kubelet Eviction Thresholds

##### Run these commands on all nodes

Unless resources are set aside for system daemons, Kubernetes pods and system daemons will compete for resources and eventually lead to resource starvation issues. Kubelet has the extra args parameter to specify eviction thresholds that trigger the kubelet to reclaim resources. 

`$ echo "KUBELET_EXTRA_ARGS=--eviction-hard=memory.available<256Mi,nodefs.available<1Gi,imagefs.available<1Gi"|sudo tee /etc/sysconfig/kubelet`

Restart kubelet service:

`$ sudo systemctl restart kubelet`

## Step5: Let iptables see Bridged Traffic

##### Run these commands on all nodes

```
$ echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee /etc/sysctl.d/k8s-iptables.conf
$ echo "net.bridge.bridge-nf-call-ip6tables=1" | sudo tee /etc/sysctl.d/k8s-ip6tables.conf
$ sudo sysctl --system
```

## Step6: Initialise the Control Plane Node
add this entry in /etc/hosts 
```
142.44.219.118 tmaster-node-1
142.44.219.120 tmaster-node-3
142.44.219.119 tmaster-node-2
```

##### Run this command on any one node, lets say `node2`

`kubeadm init --control-plane-endpoint 142.44.219.121:6446 --upload-certs`

Join Other Control Plane Nodes to the Cluster by generated command at node2.

## Step7:  Configure Kube Config and install Calico Pod Network
```
$ mkdir -p $HOME/.kube
$ sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
$ sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

`$ kubectl apply -f https://docs.projectcalico.org/archive/v3.17/manifests/calico.yaml`

## Step8: Untaint all the master

`kubectl taint nodes --all node-role.kubernetes.io/master- `
